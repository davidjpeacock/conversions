package conversions

// MetreToFoot converts metres to feet
func MetreToFoot(m Metre) Foot { return Foot(m * 3.281) }

// FootToMetre converts feet to Metres
func FootToMetre(f Foot) Metre { return Metre(f / 3.281) }

// CentimetreToInch converts centimetres to inches
func CentimetreToInch(cm Centimetre) Inch { return Inch(cm * 0.394) }

// InchToCentimetre converts inches to centimetres
func InchToCentimetre(in Inch) Centimetre { return Centimetre(in / 2.54) }

// PsiToBar converts pounds per square inch to bar
func PsiToBar(psi Psi) Bar { return Bar(psi * 0.0689476) }

// BarToPsi converts bar to pounds per square inch
func BarToPsi(bar Bar) Psi { return Psi(bar * 14.5038) }

// CelsiusToFahrenheit converts a Celsius temperature to Fahrenheit
func CelsiusToFahrenheit(c Celsius) Fahrenheit { return Fahrenheit(c*9/5 + 32) }

// FahrenheitToCelsius converts a Fahrenheit temperature to Celsius
func FahrenheitToCelsius(f Fahrenheit) Celsius { return Celsius((f - 32) * 5 / 9) }

// KelvinToCelsius converts a Kelvin temperature to Celsius
func KelvinToCelsius(k Kelvin) Celsius { return Celsius(k - 273.15) }

// CelsiusToKelvin converts a Celsius temperature to Kelvin
func CelsiusToKelvin(c Celsius) Kelvin { return Kelvin(c + 273.15) }

// KelvinToFahrenheit converts a Kelvin temperature to Fahrenheit
func KelvinToFahrenheit(k Kelvin) Fahrenheit { return Fahrenheit(k*9/5 - 459.67) }

// FahrenheitToKelvin converts a Fahrenheit temperature to Kelvin
func FahrenheitToKelvin(f Fahrenheit) Kelvin { return Kelvin((f + 459.67) * 5 / 9) }

// MPHToKMH converts miles per hour to kilometres per hour
func MPHToKMH(mph MPH) KMH { return KMH(mph*1.609)}

// KMHToMPH converts kilometres per hour to miles per hour
func KMHToMPH(kmh KMH) MPH { return MPH(kmh*0.621)}

// PintUKToLitre converts British pints to litres
func PintUKToLitre(pt PintUK) Litre { return Litre(pt*0.568)}

// PintUSToLitre converts American pints to litres
func PintUSToLitre(ptus PintUS) Litre { return Litre(ptus*0.473)}

// LitreToPintUK converts litres to British pints
func LitreToPintUK(l Litre) PintUK { return PintUK(l*1.759)}

// LitreToPintUS converts litres to American pints
func LitreToPintUS(l Litre) PintUS { return PintUS(l*2.113)}

// PintUKToPintUS converts British pints to American pints
func PintUKToPintUS(pt PintUK) PintUS { return PintUS(pt*1.200)}

// PintUSToPintUK converts American pints to British pints
func PintUSToPintUK(ptus PintUS) PintUK { return PintUK(ptus*0.832)}
